#!/bin/bash

DIR="/c/Users/matth.MATTHEW-DESKTOP/Desktop/Scripts/bash/testfolder"

echo "Checking file"

function pause() {
    read -p "$*"
}

if [ "$(ls -A $DIR)" ] 
then
    echo "Not Empty"
    cd $DIR
    dir 
    pwd
    pause "Press [Enter] to continue..."

else
    echo "Empty"
    cd $DIR
    ls 
    pwd

    pause "Press [Enter] to continue..."

fi
